#include "test_polygon.hpp"
#include "test_segment.hpp"
#include "test_measurer.hpp"
#include "test_intersector1D1D.h"
#include "CutPolygon.hpp"
#include "CutPolygon.cpp"


#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
