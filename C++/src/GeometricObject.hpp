#ifndef GEOMETRICOBJECT_HPP
#define GEOMETRICOBJECT_HPP

#include <vector>
#include "Eigen"
#include "Vertex.hpp"
#include "IGeometricObject.hpp"

using namespace std;
using namespace Eigen;

namespace GeDiM
{
  class GeometricObject : virtual public IGeometricObject
	{
		protected:
			unsigned int id;
			unsigned int globalId;

			double measure; ///< Measure of the geometric obcjet

		public:
			virtual ~GeometricObject() {}

      void SetGlobalId(const unsigned int& _globalId) override { globalId = _globalId; }
      void SetId(const unsigned int& _id) override { id = _id; }

      const unsigned int& GlobalId() override { return globalId; }
      const unsigned int& Id() override { return id; }

      //inline const double& Measure() const override { return measure; }

	};
}






#endif // GENERICOBJECT_HPP
