#ifndef IGEOMETRICOBJECT_HPP
#define IGEOMETRICOBJECT_HPP

#include <vector>

#include "Vertex.hpp"

using namespace std;

namespace GeDiM
{
  class IGeometricObject
  {
    public:
      virtual ~IGeometricObject() { }

      /// Set the global Id of the geometric object
      virtual void SetGlobalId(const unsigned int& _globalId) = 0;
      /// Set the Id of the geometric object
      virtual void SetId(const unsigned int& _id) = 0;

      /// @return The global Id of the geometric object
      virtual const unsigned int& GlobalId() = 0;
      /// @return The Id of the geometric object
      virtual const unsigned int& Id() = 0;

      /// @return The measure of the geometric object
      //virtual const double& Measure() const = 0;

      /// Computes the measure and stores it in the geometric object
      //virtual bool ComputeMeasure() = 0;
      /// Computes the measure of the geometric object
      /// @param[out] _measure : the measure barycenter
      //virtual bool ComputeMeasure(double& measure) const = 0;
  };
}

#endif // IGEOMETRICOBJECT_HPP
