#ifndef IPOLYGON_HPP
#define IPOLYGON_HPP

#include "ISegment.hpp"

namespace GeDiM
{
  class IPolygon : virtual public IGeometricObject
  {
    public:
      virtual ~IPolygon() {}

      enum Type
      {
        Unknown = 0,
        Triangle = 3,
        Square = 4,
      };

      /// Initialize the polygon with number of vertices and segments
      virtual void Initialize(const unsigned int& numberOfVertices, const unsigned int& numberOfSegments) = 0;
      /// Initialize the number of segments in the polygon
      virtual void InitializeSegments(const unsigned int& numberOfSegments) = 0;

      /// Create a new instance of the polygon
      //virtual IPolygon* Clone() const = 0;

      /// Initialize the vertices of the geometric object
      virtual void InitializeVertices(const unsigned int& numberOfVertices) = 0;
      /// Add a single vertex to the geometric object
      virtual void AddVertex(const Vertex& vertex) = 0;
      /// Add a list of vertices to the geometric object
      virtual void AddVertices(const vector<const Vertex*>& _vertices) = 0;

      /// Add a new segment in the polygon
      virtual void AddSegment(const ISegment& segment) = 0;
      /// Add a list of segments in the polygon
      virtual void AddSegments(const vector<const ISegment*>& _segments) = 0;
      /// @return The number of segments of the polygon
      virtual unsigned int NumberOfSegments() const = 0;
      /// @return The segment at position p of the polygon
      virtual const ISegment& GetSegment(const unsigned int& position) const = 0;
      /// @return The list of segments of the polygon
      virtual const vector<const ISegment*>& Segments() const = 0;

  };
}

#endif // IPOLYGON_HPP
