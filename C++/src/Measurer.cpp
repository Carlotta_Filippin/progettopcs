#include "Measurer.hpp"

namespace GeDiM
{
	// ***************************************************************************
	Measurer::Measurer(const TypeMeasurer type)
	{
		typeMeasurer = type;
		coordinateProjectionOnLine = 0.0;
		distanceSquared = 0.0;
	}

	Measurer::~Measurer()
	{

	}

    bool  Measurer::ComputeSquaredDistancePoints(const Vector3d& firstPoint, const Vector3d secondPoint)
	{
		if(typeMeasurer != Measurer::PointToPoint)
		{
           return false;
		}
		distanceSquared = (secondPoint-firstPoint).squaredNorm();
        return true;
	}

	// ***************************************************************************
    bool  Measurer::ComputeDistancePointLineAndProj(const Vector3d& point, const Vector3d& startPointLine, const Vector3d& tangentVector, const Vector3d& normalVector)
	{
		if(typeMeasurer != Measurer::PointToLine)
		{
            return false;
		}

		const Vector3d tangentVectorDifference = point - startPointLine;
		distanceSquared = tangentVectorDifference.dot(normalVector);
		distanceSquared *= distanceSquared;
		coordinateProjectionOnLine = tangentVector.dot(tangentVectorDifference) / tangentVector.squaredNorm();
        return true;
	}
}
