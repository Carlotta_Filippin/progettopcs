#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "GeometricObject.hpp"
#include "Segment.hpp"
#include "IPolygon.hpp"

using namespace std;
using namespace Eigen;

namespace GeDiM
{
  class Polygon : public GeometricObject, public IPolygon
  {
    protected:
      vector<const Vertex*> vertices; ///< Array of domain vertices
			vector< const ISegment* > segments; ///< list of the segments in the polygon

    public:
      Polygon();
      Polygon(const unsigned int& _id) : Polygon() { globalId = _id; id = _id;}
      virtual ~Polygon();

      inline void Initialize(const unsigned int& numberOfVertices,
                             const unsigned int& numberOfSegments)
      {
        InitializeVertices(numberOfVertices);
        InitializeSegments(numberOfSegments);
      }

      inline void GetPolygon(const Polygon& polygon)
      {
        polygon.Vertices();
        polygon.Segments();
      }

    //  inline IPolygon* Clone() const { return new Polygon(); }

      inline void InitializeVertices(const unsigned int& numberOfVertices) { vertices.reserve(numberOfVertices); }
      inline void AddVertex(const Vertex& vertex) { vertices.push_back(&vertex); }
      inline void AddVertices(const vector<const Vertex*>& _vertices) { vertices = _vertices; }
      inline unsigned int NumberOfVertices() const { return vertices.size(); }
      inline const Vertex& GetVertex(const unsigned int& position) const { return *vertices[position]; }
      inline const vector<const Vertex*>& Vertices() const { return vertices; }

      inline void InitializeSegments(const unsigned int& numberOfSegments) { segments.reserve(numberOfSegments);}
      inline void AddSegment(const ISegment& segment) { segments.push_back(&segment); }
      inline void AddSegments(const vector<const ISegment*>& _segments) {segments = _segments; }
      inline unsigned int NumberOfSegments() const { return segments.size(); }
      inline const ISegment& GetSegment(const unsigned int& position) const { return *segments[position]; }
      inline const vector<const ISegment*>& Segments() const { return segments; }

  };
}
#endif //POLYGON_HPP
