#include "CutPolygon.hpp"

using namespace std;

namespace GeDiM
 {
   Output::ExitCodes ConcaveToConvex::ConcaveToConvexPolygon(const IPolygon& polygon, list<IPolygon*>& polygons, const ISegment& Line, const double& tolerance)
  {
    Intersector1D1D IntersectionLineSegments;
    vector<Vertex> PointsIntersection;
    vector<Segment> newSegments;
    vector<Segment> SegsLine;
    vector<Vertex> newVertices;
    vector<Polygon> tmpPolygon;
    vector<Vertex> cuttedPolygonVertices;
    vector<Segment> cuttedPolygonSegments;
    vector<Segment> newPolygonSegments;
    vector<Vertex> newPolygonVertices;
    Polygon* newPolygon = new Polygon;
    Polygon* cuttedPolygon = new Polygon;

    //ciclo per determinare i punti di intersezione
    for (unsigned int i=0; i<polygon.NumberOfSegments(); i++){

        newVertices.push_back(polygon.GetSegment(i).Origin());

        const Segment& segment = static_cast<const Segment&>(polygon.GetSegment(i));  //serve per accedere agli attributi
        IntersectionLineSegments.SetFirstTangentVector(segment.Tangent());
        IntersectionLineSegments.SetSecondTangentVector(Line.Tangent());
        IntersectionLineSegments.ComputeIntersectionEdges(segment.Tangent(), Line.Tangent(),segment.Origin()-Line.Origin());//calcolo intersezioni con Intersector1D1D

        if(IntersectionLineSegments.TypeIntersection() == Intersector1D1D::Type::IntersectionOnSegment){

            Vector2d IntersPoint = IntersectionLineSegments.resultParametricCoordinates;
            PointsIntersection.push_back(IntersPoint);

            newVertices.push_back(IntersPoint); //creo vettore dei punti di intersezione
        }

    }
        //creo vettore per salvare i nuovi segmenti
        vector<const ISegment*> SegmentiIniz = polygon.Segments();
        bool isBetween=false;

        for(unsigned int j=0; j<SegmentiIniz.size(); j++){
        const Segment& tmpsegment= static_cast<const Segment&>(*SegmentiIniz[j]);
        Segment original = tmpsegment;
        for(unsigned int i=0;i<PointsIntersection.size();i++){
            if (original.PointPosition(PointsIntersection[i]) == ISegment::Position::Between){
                isBetween=true;
                Segment s1, s2;//quando ho intersezione sostisuisco il vecchio segmento con i due nuovi segmenti
                s1.AddVertex(original.End());
                s1.AddVertex(PointsIntersection[i]);
                s2.AddVertex(PointsIntersection[i]);
                s2.AddVertex(original.Origin());
                newSegments.push_back(s1);
                newSegments.push_back(s2);
            }
        }

        if(!isBetween){
            newSegments.push_back(original); //se non c'è intersezione nel segmento salviamo quello vecchio
        } else
            isBetween=false;  //settiamo questo booleano a falso per il prossimo segmento nel ciclo
    }
    // aggiungo i segmenti ai nuovi poligoni(a sx) creati dal taglio
    for (unsigned int i=0; i< newSegments.size();i++){
            unsigned int j=0;
        while(j<PointsIntersection.size()-1){  //fino che non raggiungo l'ultimo punto di intersezione
            if (newSegments[i].Origin() == PointsIntersection[j]){
                newPolygonSegments.push_back(newSegments[i]);              //prima aggiungo quelli che appartengono al perimetro del vecchio poligono
                for(unsigned int n=i+1; n<newSegments.size();n++){
                    if(newSegments[n].Origin() != PointsIntersection[j])
                        newPolygonSegments.push_back(newSegments[n]);
                }
               Segment SegLine;                                     //poi aggiungo i nuovi segmenti che giaciono sulla retta di taglio
               SegLine.AddVertex(PointsIntersection[j+1]);
               SegLine.AddVertex(PointsIntersection[j]);
               newPolygonSegments.push_back(SegLine);
               SegsLine.push_back(SegLine);

               for(unsigned k=0;k<newPolygonSegments.size();k++){
                newPolygonVertices.push_back(newPolygonSegments[k].Origin());
                newPolygonVertices.push_back(newPolygonSegments[k].End());
               }

              newPolygon->Initialize(newPolygonVertices.size(), newPolygonSegments.size());

            for(unsigned int i=0; i<newPolygonVertices.size(); i++){
                newPolygon->AddVertex(newPolygonVertices[i]);}

            for(unsigned int i=0;i<newPolygonSegments.size();i++){
                newPolygon->AddSegment(newPolygonSegments[i]);}

             j++;
            }
        }
        polygons.push_back(newPolygon);
    }


        const Segment& line= static_cast<const Segment&>(Line);
        // se i poligoni "staccati" dal poligono originale sono a sinistra della retta di taglio
        for (unsigned int i=0; i<newVertices.size(); i++){
            if(line.PointPosition(newVertices[i]) == ISegment::Position::AtTheRight || line.PointPosition(newVertices[i]) == ISegment::Position::Between) //allora aggiungo tutti i vertici che sono a dx o sulla retta all'ultimo poligono
                cuttedPolygonVertices.push_back(newVertices[i]);
        }

        for (unsigned int i=0; i<cuttedPolygonVertices; i++){
            Segment segment;
            segment.AddVertex(cuttedPolygonVertices[i]);
            segment.AddVertex(cuttedPolygonVertices[i+1]);
            cuttedPolygonSegments.push_back(segment);
        }

    cuttedPolygon->Initialize(cuttedPolygonVertices.size(), cuttedPolygonSegments.size());

    for(unsigned int i=0; i<cuttedPolygonVertices.size(); i++){
        cuttedPolygon->AddVertex(cuttedPolygonVertices[i]);
    }

    for(unsigned int i=0;i<cuttedPolygonSegments.size();i++){
        cuttedPolygon->AddSegment(cuttedPolygonSegments[i]);
    }

    polygons.push_back(cuttedPolygon);

    int counter=1;
    auto x = "Il poligono viene tagliato nel seguente modo : ";
    for (list<IPolygon*>::iterator it = polygons.begin(); it != polygons.end(); it++)
        {
          if (*it == NULL)
          {
            Output::PrintErrorMessage("Polygon %d is NULL", true, counter);
            continue;
          }

          const IPolygon& polygon = **it;

         //if ((ConcaveToConvex::IsPolyhedronConvex(cuttedPolygon, tolerance))==false) {
             // polygons.erase(it);
             // ConcaveToConvex::ConcaveToConvexPolygon(cuttedPolygon,polygons, tolerance);

          //} else {
          Output::Assert(ConcaveToConvex::ConcaveToConvexPolygon(polygon, tolerance), "%s: IsPolyhedronConvex %d", x, counter);

            for(unsigned int i=0; i< polygon.NumberOfVertices(); i++){
            cout<<polygon.GetVertex(i)<< " \n "<< " sono le coordinate del vertice "<<i<< " del poligono "<<counter<<endl;
            }


            Output::PrintLine('*');
            counter++;

            }

          return Output::Success;
        }