#ifndef __CUT_POLYGON_H
#define __CUT_POLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "IPolygon.hpp"


namespace GeDiM
{
    class PolygonCut;

    class PolygonCut
    {
        public:
              /// Function to cut a generic polygon to a list of  polygons
              /// @param[in] originalPolygon - the  polygon
              /// @param[in] tolerance - the tolerance for computations
              /// @param[out] cuttedPolygons - the list of cutted polygons
              static bool CutPolygon(const IPolygon& originalPolygon, list<IPolygon*>& cuttedPolygons, const double& tolerance);

    };
}

#endif // __CUT_POLYGON_H
