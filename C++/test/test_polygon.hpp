#ifndef __TEST_POLYGON_H
#define __TEST_POLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Polygon.hpp"

using namespace GeDiM;
using namespace testing;
using namespace std;

namespace PolygonTesting {

void FillPolygonVertices(vector<Vertex>& vertices)
{
  unsigned int numVertices = 10;
  vertices.reserve(numVertices);
  vertices.push_back(Vertex(1.0000e+00, 2.0000e+00));
  vertices.push_back(Vertex(7.6489e-01, 1.3236e+00));
  vertices.push_back(Vertex(4.8943e-02, 1.3090e+00));
  vertices.push_back(Vertex(6.1958e-01, 8.7639e-01));
  vertices.push_back(Vertex(4.1221e-01, 1.9098e-01));
  vertices.push_back(Vertex(1.0000e+00, 6.0000e-01));
  vertices.push_back(Vertex(1.5878e+00, 1.9098e-01));
  vertices.push_back(Vertex(1.3804e+00, 8.7639e-01));
  vertices.push_back(Vertex(1.9511e+00, 1.3090e+00));
  vertices.push_back(Vertex(1.2351e+00, 1.3236e+00));
}

void FillPolygonSegments(vector<Vertex>& vertices, vector<ISegment*>& segments)
{
    unsigned int numVertices = 10, numSegments = 10;

    //polygon.Initialize(numVertices, numSegments);

    vertices.reserve(numVertices);
    vertices.push_back(Vertex(1.0000e+00, 2.0000e+00));
    vertices.push_back(Vertex(7.6489e-01, 1.3236e+00));
    vertices.push_back(Vertex(4.8943e-02, 1.3090e+00));
    vertices.push_back(Vertex(6.1958e-01, 8.7639e-01));
    vertices.push_back(Vertex(4.1221e-01, 1.9098e-01));
    vertices.push_back(Vertex(1.0000e+00, 6.0000e-01));
    vertices.push_back(Vertex(1.5878e+00, 1.9098e-01));
    vertices.push_back(Vertex(1.3804e+00, 8.7639e-01));
    vertices.push_back(Vertex(1.9511e+00, 1.3090e+00));
    vertices.push_back(Vertex(1.2351e+00, 1.3236e+00));

    //for (unsigned int s = 0; s < numVertices; s++)
    //    polygon.AddVertex(vertices[s]);

    segments.reserve(numSegments);

    for (unsigned int v = 0; v < numVertices; v++)
    {
        segments.push_back(new Segment(v + 1));

        ISegment& segment = *segments[v];
        const Vertex& origin = vertices[v];
        const Vertex& end = vertices[(v + 1) % numVertices];
        segment.Initialize(origin, end);
    }

}
//inline void InitializeVertices(const unsigned int& numberOfVertices) { vertices.reserve(numberOfVertices); }

TEST(TestPolygon, TestInitializeVertices)
{
    vector<Vertex> vertices;
    FillPolygonVertices(vertices);

    Polygon polygon;

    try
    {
        polygon.InitializeVertices(10);
        EXPECT_EQ(polygon.NumberOfVertices(), 1);
    }
    catch (const exception& exception)
    {
        FAIL();
    }
}

TEST(TestPolygon, TestInitializeSegments)
{
    vector<Vertex> vertices;
    vector<ISegment*> segments;
    FillPolygonSegments(vertices, segments);

    Polygon polygon;
    unsigned int numSegments = 10;
    try
    {
        polygon.InitializeSegments(numSegments);
        EXPECT_EQ(polygon.Segments(), 1);
    }
    catch (const exception& exception)
    {
        FAIL();
    }
}

TEST(TestPolygon, TestGetVertex)
{
  vector<Vertex> vertices;
  FillPolygonVertices(vertices);

  Polygon polygon;
  unsigned int numVertices = 10;
  polygon.InitializeVertices(numVertices);
  polygon.AddVertices(vertices);

  try
  {
    const Vertex& vertex = polygon.GetVertex(2);
    EXPECT_EQ(vertex.x, vertices[2].x());
    EXPECT_EQ(vertex.y, vertices[2].y());
  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestPolygon, TestVertices)
{
    vector<Vertex> vertices;
    FillPolygonVertices(vertices);

    try
    {
        Polygon polygon;
        unsigned int numVertices = 10;
        polygon.InitializeVertices(numVertices);

        EXPECT_EQ(polygon.NumberOfVertices(), 10);

    }
    catch (const exception& exception)
    {
        FAIL();
    }
}

TEST(TestPolygon, TestGetSegment)
{
  vector<Vertex> vertices;
  vector<ISegment*> segments;
  FillPolygonSegments(vertices, segments);

  Polygon polygon;
  unsigned int numSegments = 10;
  polygon.InitializeSegments(numSegments);
  polygon.AddSegments(segments);

  try
  {
    EXPECT_EQ(polygon.GetSegment(3).Origin().x, vertices[3].x());
    EXPECT_EQ(polygon.GetSegment(3).Origin().y, vertices[3].y());
    EXPECT_EQ(polygon.GetSegment(3).End().x, vertices[4].x());
    EXPECT_EQ(polygon.GetSegment(3).End().y, vertices[4].y());
  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestPolygon, TestGetPolygonFailed)
{
  Polygon polygon;

  try
  {
    polygon.GetPolygon(12);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }
}

TEST(TestPolygon, TestGetVertexFailed)
{
  vector<Vertex> vertices;
  FillPolygonVertices(vertices);

  Polygon polygon;
  //unsigned int numVertices = 10;
  //polygon.InitializeVertices(numVertices);
  //polygon.AddVertices(vertices);

  try
  {
    //polygon.GetVertex(12, 1);
    polygon.GetVertex(12);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    //polygon.GetVertex(1, 17);
    polygon.GetVertex(17);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Vertex not found"));
  }
}

TEST(TestPolygon, TestGetSegmentFailed)
{
  vector<Vertex> vertices;
  vector<ISegment*> segments;
  FillPolygonSegments(vertices, segments);

  Polygon polygon;
  unsigned int numSegments = 10;
  polygon.InitializeSegments(numSegments);

  try
  {
    //polygon.Getsegment(12, 3);
    polygon.GetSegment(12);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Polygon not found"));
  }

  try
  {
    //polygon.GetSegment(1, 17);
      polygon.GetSegment(17);
    FAIL();
  }
  catch (const exception& exception)
  {
    EXPECT_THAT(std::string(exception.what()), Eq("Segment not found"));
  }

}

}

#endif // __TEST_POLYGON_H
