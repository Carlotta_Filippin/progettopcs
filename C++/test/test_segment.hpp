#ifndef __TEST_SEGMENT_H
#define __TEST_SEGMENT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Segment.hpp"

using namespace GeDiM;
using namespace testing;
using namespace std;


    //inline void Initialize(const Vertex& origin, const Vertex& end)
    //{ vertices.push_back(&origin); vertices.push_back(&end); }
void FillSegmentVertices(vector<Vertex> vertices)
{
  unsigned int numVertices = 2;
  vertices.reserve(numVertices);
  vertices.push_back(Vertex(1.0000e+00, 2.0000e+00));
  vertices.push_back(Vertex(7.6489e-01, 1.3236e+00));

}


TEST(TestSegment,TestInitialize)
  {
    vector<Vertex> vertices;
    FillSegmentVertices(vertices);

    Segment segment;
    Vertex origin = Vertex(1.0000e+00, 2.0000e+00);
    Vertex end = Vertex(7.6489e-01, 1.3236e+00);
    try {
        segment.Initialize(origin, end);
       EXPECT_EQ(segment.Origin(), vertices[0]);
       EXPECT_EQ(segment.End(), vertices[1]);

    }  catch (const exception& exception) {
       FAIL();
    }
  }

//inline void InitializeVertices(const unsigned int& numberOfVertices) { vertices.reserve(numberOfVertices); }

TEST(TestSegment, TestInitializeVertices)
{
    vector<Vertex> vertices;
    FillSegmentVertices(vertices);

    Segment segment;
    unsigned int numVertices=2;
    try {
        segment.InitializeVertices(numVertices);
        EXPECT_EQ(segment.NumberOfVertices() , 1);
    }  catch (const exception& exception) {
        FAIL();
    }

}

//inline void AddVertex(const Vertex& vertex) { vertices.push_back(&vertex); }

TEST(TestSegment, TestAddVertex)
{
    vector<Vertex> vertices;
    FillSegmentVertices(vertices);

    Segment segment;
    Vertex vertex=(3.0000e+00, 4.0000e+00); //magari non gli piace il punto (?)
    try {
        segment.AddVertex(vertex);
        EXPECT_EQ(segment.AddVertex(vertex);
        {
    }  catch (const exception& exception) {
        FAIL();
    }
}

//inline void AddVertices(const vector<const Vertex*>& _vertices) { vertices = _vertices; }

TEST(TestSegment, TestAddVertices)
{
    vector<Vertex> vertices;
    FillSegmentVertices(vertices);
    vector _Vertices//= non so che mettere (?)

    Segment segment;
    int n=numOfVertices;
    for (int i=0; i<n; i++)
        {try {
            segment.AddVertex(vertex);
            EXPECT_EQ(segment.AddVertex);
            }  catch (const exception& exception) {
            FAIL();
            }
        }
}



//inline unsigned int NumberOfVertices() const { return vertices.size(); }

TEST(TestSegment, TestNumberOfVertices)
{
    vector <Vertex> vertices;
    FillSegmentVertices(vertices);

    Segment segment;
    try {
        NumberOfVertices(vertices.size());
        EXPECT_EQ(segment.NumberOfVertices);
    }  catch (const exception& exception) {
       FAIL();
    }
}



//inline const Vertex& GetVertex(const unsigned int& position) const { return *vertices[position]; }




//inline const vector<const Vertex*>& Vertices() const { return vertices; }










#endif // __TEST_SEGMENT_H
