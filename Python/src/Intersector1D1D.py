import numpy as np
from enum import Enum
import src.Vertex as Vertex
import src.Segment as Segment
import math

class Intersector1D1D:

    def __init__(self, toleranceParallelism: float, toleranceIntersection: float, matrixTangentVector: np.ndarray):
        self.__toleranceParallelism = toleranceParallelism
        self.__toleranceIntersection = toleranceIntersection
        self.firstSegment = firstSegment
        self.secondSegment = secondSegment

        Enum Type:
    noIntersection = 0
    intersectionOnLine = 1
    intersectionOnSegment = 2
    intersectionParallelOnLine = 4

    Enum Position:
    Begin = 0
    Inside = 1
    End = 2
    Outside = 3

    def SetToleranceParallelism(self, _tolerance: float):
        self.__toleranceParallelism = _tolerance
        pass
    def SetToleranceIntersection(self, _tolerance: float):
        self.__toleranceIntersection = _tolerance
        pass

    def SetMatrixTangentVector(self, _vector0: np.array(), _vector1: np.array()):
        self.__matrixTangentVector
        pass

    def SetFirstTangentVector(self):
        pass

    def SetSecondTangentVector(self):
        pass

    def ComputeIntersectionEdges(self):
        pass

    def ToleranceIntersection(self):
        pass

    def ToleranceParallelism(self):
        pass

    def ParametricCoordinates(self):
        self

    def FirstParametricCoordinate(self):
        pass

    def SecondParametricCoordinate(self):
        pass

    def PositionIntersectionInFirstEdge(self):
        pass

    def PositionIntersectionInSecondEdge(self):
        pass

    def TypeIntersection(self):
        pass

    def IntersectionStartPointFirstVector(self):
        pass

    def IntersectionStartPointSecondVector(self):
        pass

    def ParamCoordSetIntersection(self):
        pass


