import src.Vertex as Vertex
import src.Segment as Segment


def add_segment(segments: list, segment: Segment):
    segments.append(segment)
    pass


class Polygon:
    def __init__(self, vertices: list, segments: list):
        self._vertices = vertices
        self._segments = segments
        pass

    @staticmethod
    def add_vertex(self, vertices: list, vertex: Vertex):
        vertices.append(vertex)
        pass

    def add_vertices(self, vertices: list):
        self._vertices = vertices
        pass

    def add_segments(self, segments:list):
        self._segments = segments
        pass

    def number_of_vertices(self):
        return len(self._vertices)

    def number_of_segments(self):
        return len(self._segments)

    def get_vertex(self, position: int):
        return self._vertices[position]

    def vertices(self):
        return self._vertices

    def segments(self):
        return self._segments

    def get_segment(self, position: int):
        return self._segments[position]
