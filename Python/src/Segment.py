import src.Vertex as Vertex
import math
import numpy as np


class Segment:
    def __init__(self, begin: Vertex, end: Vertex):
        self._begin = begin
        self._end = end

    def add_vertices(self, begin: Vertex, end: Vertex):
        self._begin = begin
        self._end = end
        pass

    def get_begin(self):
        return self._begin

    def get_end(self):
        return self._end

    def norm(self):
        norm2 = pow((self._end-self._begin).x, 2) + pow((self._end-self._begin).y, 2)
        return math.sqrt(norm2)

    @property
    def compute_tangent(self):
        tangent = self._end - self._begin
        x = tangent.x / tangent.norm()
        y = tangent.y / tangent.norm()
        return x, y

    def compute_position(self, point: Vertex):
        tolerance = 1.0e-16
        tangent_vector_edge = self._end-self._begin
        tangent_vector_diff = point - self._begin
        cross_prod = tangent_vector_edge.x * tangent_vector_diff.y - tangent_vector_diff.x - tangent_vector_edge.y
        coordinate_curvilinear = np.inner(tangent_vector_edge, tangent_vector_diff) / tangent_vector_edge.norm()
        if cross_prod > tolerance:
            return 'AtTheLeft'
        if cross_prod < - tolerance:
            return 'AtTheRight'
        if coordinate_curvilinear < - tolerance:
            return 'Behind'
        if coordinate_curvilinear > 1.0 + tolerance:
            return 'Beyond'
        if abs(coordinate_curvilinear) < tolerance:
            return 'AtTheOrigin'
        if abs(coordinate_curvilinear) > 1.0 - tolerance:
            return 'AtTheEnd'
        return 'Between'
