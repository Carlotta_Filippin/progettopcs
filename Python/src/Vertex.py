

class Vertex:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def X(self):
        return self.x

    def Y(self):
        return self.y

