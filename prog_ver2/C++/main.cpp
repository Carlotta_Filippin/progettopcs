#include "test_cut.hpp"
#include "test_mesh.hpp"
#include "test_geometryFactory.h"
#include "test_intersector1D1D.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
