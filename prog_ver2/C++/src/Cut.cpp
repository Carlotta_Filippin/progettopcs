#include "Cut.hpp"
#include "Intersector1D1D.h"
#include "GeometryFactory.h"
#include <list>

using namespace std;
namespace GeDiM {


vector<Vector2d> Cut::CutPolygon(const vector<Vector2d>& points, const Vector2d& origin, const Vector2d& end, bool side)
{
    Intersector1D1D Intersection;
    vector<Vector2d> intersectionPoints;
    vector<Vector2d> newPoints;
    vector<Vector2d> counterwiseVertices;
    vector<int> newPolygonVertices;
    vector<Vector2d> cuttedPolygon;
    vector<Vector2d> reverseCuttedPolygons;
    vector<int> cuttedPolygons;
    vector<int> numReverse;
    vector<Vector2d> resultPolygons;


    //aggiungo a NewPoints i vertici originali
    for(unsigned int i=0; i<points.size();i++){
        newPoints.push_back(points[i]);
    }

     // calcolo i punti di intersezione

    Intersection.SetFirstSegment(origin,end);

    for(unsigned int i=0; i<points.size()-1; i++){
        counterwiseVertices.push_back(points[i]);

        Intersection.SetSecondSegment(points[i], points[i+1]);
        Intersection.ComputeIntersection();
        if(Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnLine || Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnSegment){
            Vector2d param;
            Vector2d point;
            Vector2d t;
            t[0] = end[0]-origin[0];
            t[1] = end[1]-origin[1];

            param = Intersection.ParametricCoordinates();
            point[0] = origin[0] + t[0]*param[0];
            point[1] = origin[1] + t[1]*param[0];

            if(point != points[i] && point != points[i+1]){
                intersectionPoints.push_back(point);
                counterwiseVertices.push_back(point);
            }
        }
    }

    //controllo ultimo segmento
    counterwiseVertices.push_back(points[points.size()-1]);
    Intersection.SetSecondSegment(points[points.size()-1],points[0]);
    Intersection.ComputeIntersection();
    if(Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnLine || Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnSegment){
        Vector2d param;
        Vector2d point;
        Vector2d t;
        t[0] = end[0]-origin[0];
        t[1] = end[1]-origin[1];

        param = Intersection.ParametricCoordinates();
        point[0] = origin[0] + t[0]*param[0];
        point[1] = origin[1] + t[1]*param[0];

        if(point != points[points.size()-1] && point != points[0]){
            intersectionPoints.push_back(point);
            counterwiseVertices.push_back(point);
        }
    }

    //aggiungo a NewPoints i punti di intersezione e gli estremi del segmento di taglio

    if(intersectionPoints[0][0]<origin[0] && intersectionPoints[intersectionPoints.size()-1][0]>end[0]){
        newPoints.push_back(intersectionPoints[0]);
        newPoints.push_back(origin);
        for(unsigned int i=1; i<intersectionPoints.size()-1;i++){
            newPoints.push_back(intersectionPoints[i]);
        }
        newPoints.push_back(end);
        newPoints.push_back(intersectionPoints[intersectionPoints.size()-1]);
    }
    if(intersectionPoints[0][0]>end[0] && intersectionPoints[intersectionPoints.size()-1][0]<origin[0]){
            newPoints.push_back(intersectionPoints[0]);
            newPoints.push_back(end);
       for(unsigned int i=1; i<intersectionPoints.size()-1;i++){
           newPoints.push_back(intersectionPoints[i]);
        }
        newPoints.push_back(origin);
        newPoints.push_back(intersectionPoints[intersectionPoints.size()-1]);
    }
    if(intersectionPoints.size()==1){
        newPoints.push_back(end);
        newPoints.push_back(origin);
        newPoints.push_back(intersectionPoints[0]);
    }


    //assegno al poligono tagliato i vertici da newPoints
    Vector2d segmento;
    segmento = origin-end;
    for(unsigned int i=0; i<newPoints.size();i++){
        Vector2d differenza;
        differenza = origin-newPoints[i];
        double determinante = (segmento[0]*differenza[1]) - differenza[0]*segmento[1];
        double tollerance = 1E-14;

        if(determinante<=tollerance)
            cuttedPolygon.push_back(newPoints[i]);
            
        //assegno vertici agli altri poligoni
        if(determinante>=-tollerance)
            reverseCuttedPolygons.push_back(newPoints[i]);
      }

    //numero i vertici del poligono tagliato
    for(unsigned int i=0; i<cuttedPolygon.size(); i++){
        for(unsigned int j=0; j<newPoints.size(); j++){
            if(cuttedPolygon[i]==newPoints[j])
             cuttedPolygons.push_back(j);
        }
    }

    //numero i vertici dei poligoni "scartati"
    if(intersectionPoints.size()<=2){
        for(unsigned int i=0; i<reverseCuttedPolygons.size(); i++){
            for(unsigned int j=0; j<newPoints.size(); j++){
                if(reverseCuttedPolygons[i]==newPoints[j])
                 cuttedPolygons.push_back(j);
            }
        }
    }

    if(intersectionPoints.size()>2){
        resultPolygons.push_back(reverseCuttedPolygons[2]);
        resultPolygons.push_back(reverseCuttedPolygons[0]);
        resultPolygons.push_back(reverseCuttedPolygons[4]);
        resultPolygons.push_back(reverseCuttedPolygons[3]);

        resultPolygons.push_back(reverseCuttedPolygons[5]);
        resultPolygons.push_back(reverseCuttedPolygons[1]);
        resultPolygons.push_back(reverseCuttedPolygons[7]);
        resultPolygons.push_back(reverseCuttedPolygons[6]);


        //numero i vertici dei poligoni "scartati"
        for(unsigned int i=0; i<resultPolygons.size(); i++){
            for(unsigned int j=0; j<newPoints.size(); j++){
                if(resultPolygons[i]==newPoints[j])
                 cuttedPolygons.push_back(j);
            }
        }

    }

    cout<< "the collection of resulting new points created concatenated with the original collection is:"<<endl;
    for(unsigned int i=0; i<newPoints.size();i++){
        cout<< "Point"<< i <<endl;
        cout<< newPoints[i][0]<<"\t"<< newPoints[i][1]<<endl;
    }
    cout<<"the resulting N polygons derived from the cutting process as a collection of vertices indices ordered counterclockwise are:"<<endl;
    if(intersectionPoints.size()<=2){
        cout<<"Polygon 1:"<<endl;
        for(unsigned int i=0;i<cuttedPolygon.size();i++)
            cout<<cuttedPolygons[i]<<endl;
        cout<<"Polygon 2:"<<endl;
        for(unsigned int i=cuttedPolygon.size();i<cuttedPolygons.size();i++)
            cout<<cuttedPolygons[i]<<endl;
    }
    if(intersectionPoints.size()>2){
        cout<<"Polygon 1:"<<endl;
        for(unsigned int i=0;i<cuttedPolygon.size();i++)
            cout<<cuttedPolygons[i]<<endl;
        cout<<"Polygon 2:"<<endl;
        for(unsigned int i=cuttedPolygon.size();i<cuttedPolygons.size()-4;i++)
            cout<<cuttedPolygons[i]<<endl;
        cout<<"Polygon 3:"<<endl;
        for(unsigned int i=cuttedPolygons.size()-4;i<cuttedPolygons.size();i++)
            cout<<cuttedPolygons[i]<<endl;
    }

    if(side)
        return cuttedPolygon;
    if(!side)
        return reverseCuttedPolygons;
    else
        return newPoints;
}

vector<Vector2d> Cut::CutPolygon2(const vector<Vector2d> &points, const Vector2d &origin, const Vector2d &end, bool side)
{

        Intersector1D1D Intersection;
        vector<Vector2d> intersectionPoints;
        vector<Vector2d> newPoints;
        vector<Vector2d> counterwiseVertices;
        vector<int> newPolygonVertices;
        vector<Vector2d> cuttedPolygon;
        vector<Vector2d> reverseCuttedPolygons;
        vector<int> cuttedPolygons;
        vector<int> numReverse;
        vector<Vector2d> resultPolygons;


        //aggiungo a NewPoints i vertici originali
        for(unsigned int i=0; i<points.size();i++){
            newPoints.push_back(points[i]);
        }

         // calcolo i punti di intersezione

        Intersection.SetFirstSegment(origin,end);

        for(unsigned int i=0; i<points.size()-1; i++){
            counterwiseVertices.push_back(points[i]);

            Intersection.SetSecondSegment(points[i], points[i+1]);
            Intersection.ComputeIntersection();
            if(Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnLine || Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnSegment){
                Vector2d param;
                Vector2d point;
                Vector2d t;
                t[0] = end[0]-origin[0];
                t[1] = end[1]-origin[1];

                param = Intersection.ParametricCoordinates();
                point[0] = origin[0] + t[0]*param[0];
                point[1] = origin[1] + t[1]*param[0];

                if(point != points[i] && point != points[i+1]){
                    intersectionPoints.push_back(point);
                    counterwiseVertices.push_back(point);
                }
            }
        }

        //controllo ultimo segmento
        counterwiseVertices.push_back(points[points.size()-1]);
        Intersection.SetSecondSegment(points[points.size()-1],points[0]);
        Intersection.ComputeIntersection();
        if(Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnLine || Intersection.TypeIntersection() == Intersector1D1D::Type::IntersectionOnSegment){
            Vector2d param;
            Vector2d point;
            Vector2d t;
            t[0] = end[0]-origin[0];
            t[1] = end[1]-origin[1];

            param = Intersection.ParametricCoordinates();
            point[0] = origin[0] + t[0]*param[0];
            point[1] = origin[1] + t[1]*param[0];

            if(point != points[points.size()-1] && point != points[0]){
                intersectionPoints.push_back(point);
                counterwiseVertices.push_back(point);
            }
        }

        for(unsigned int i=0; i<intersectionPoints.size();i++)
            newPoints.push_back(intersectionPoints[i]);

        //assegno al poligono tagliato i vertici da newPoints
        Vector2d segmento;
        segmento = origin-end;
        for(unsigned int i=0; i<newPoints.size();i++){
            Vector2d differenza;
            differenza = origin-newPoints[i];
            double determinante = (segmento[0]*differenza[1]) - differenza[0]*segmento[1];
            double tollerance = 1E-14;

            if(determinante<=tollerance)
                cuttedPolygon.push_back(newPoints[i]);

            //assegno vertici agli altri poligoni
            if(determinante>=-tollerance)
                reverseCuttedPolygons.push_back(newPoints[i]);
          }

        //numero i vertici del poligono tagliato
        for(unsigned int i=0; i<cuttedPolygon.size(); i++){
            for(unsigned int j=0; j<newPoints.size(); j++){
                if(cuttedPolygon[i]==newPoints[j])
                 cuttedPolygons.push_back(j);
            }
        }

        //numero i vertici dei poligoni "scartati"
        if(intersectionPoints.size()<=2){
            for(unsigned int i=0; i<reverseCuttedPolygons.size(); i++){
                for(unsigned int j=0; j<newPoints.size(); j++){
                    if(reverseCuttedPolygons[i]==newPoints[j])
                     cuttedPolygons.push_back(j);
                }
            }
        }

        if(intersectionPoints.size()>2){
            resultPolygons.push_back(reverseCuttedPolygons[2]);
            resultPolygons.push_back(reverseCuttedPolygons[0]);
            resultPolygons.push_back(reverseCuttedPolygons[4]);
            resultPolygons.push_back(reverseCuttedPolygons[3]);

            resultPolygons.push_back(reverseCuttedPolygons[5]);
            resultPolygons.push_back(reverseCuttedPolygons[1]);
            resultPolygons.push_back(reverseCuttedPolygons[7]);
            resultPolygons.push_back(reverseCuttedPolygons[6]);


            //numero i vertici dei poligoni "scartati"
            for(unsigned int i=0; i<resultPolygons.size(); i++){
                for(unsigned int j=0; j<newPoints.size(); j++){
                    if(resultPolygons[i]==newPoints[j])
                     cuttedPolygons.push_back(j);
                }
            }

        }

        if(side)
            return cuttedPolygon;
        if(!side)
            return reverseCuttedPolygons;
        else
            return newPoints;
    }



}
