#ifndef CUT_H
#define CUT_H

#include "GeometryFactory.h"
#include "Intersector1D1D.h"

using namespace GeometryFactoryLibrary;

namespace GeDiM {

  class Cut;

  class Cut {
  private:

    public:

      vector<Vector2d> CutPolygon(const vector<Vector2d>& points, const  Vector2d& origin, const Vector2d& end, bool side);
      vector<Vector2d> CutPolygon2(const vector<Vector2d> &points, const Vector2d &origin, const Vector2d &end, bool side);
  
  };
}

#endif // CUT_H
