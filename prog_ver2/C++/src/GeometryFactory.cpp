#include "GeometryFactory.h"

namespace GeometryFactoryLibrary {

  int GeometryFactory::CreatePolygon(const vector<Vector2d>& vertices)
  {
    int polygonId = _polygons.size() + 1;
    _polygons.insert(pair<int, Polygon>(polygonId, Polygon()));
    _polygonVertices.insert(pair<int, vector<const Vector2d*>>(polygonId, vector<const Vector2d*>()));
    _polygonEdges.insert(pair<int, vector<const Segment*>>(polygonId, vector<const Segment*>()));

    vector<const Vector2d*>& polygonVertices = _polygonVertices[polygonId];
    vector<const Segment*>& polygonEdges = _polygonEdges[polygonId];

    unsigned int numVertices = vertices.size();

    polygonVertices.reserve(numVertices);
    for (unsigned int v = 0; v < numVertices; v++)
    {
      const Vector2d& vertex = vertices[v];

      _points.push_back(Vector2d());
      Vector2d& point = _points.back();
      point[0] = vertex[0];
      point[1] = vertex[1];

      polygonVertices.push_back(&point);
    }

    polygonEdges.reserve(numVertices);
    for (unsigned int e = 0; e < numVertices; e++)
    {
      const Vector2d& from = *polygonVertices[e];
      const Vector2d& to = *polygonVertices[(e + 1) % numVertices];

      _segments.push_back(Segment(from, to));
      Segment& segment = _segments.back();
      polygonEdges.push_back(&segment);
    }

    return polygonId;
  }

  const Polygon& GeometryFactory::GetPolygon(const int& polygonId)
  {
    const auto& polygonIterator = _polygons.find(polygonId);

    if (polygonIterator == _polygons.end())
      throw runtime_error("Polygon not found");

    return polygonIterator->second;
  }

  int GeometryFactory::GetPolygonNumberVertices(const int& polygonId)
  {
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end())
      throw runtime_error("Polygon not found");

    const vector<const Vector2d*>& polygonVertices = polygonIterator->second;
    return polygonVertices.size();
  }

  const Vector2d& GeometryFactory::GetPolygonVertex(const int& polygonId, const int& vertexPosition)
  {
    const auto& polygonIterator = _polygonVertices.find(polygonId);

    if (polygonIterator == _polygonVertices.end())
      throw runtime_error("Polygon not found");

    const vector<const Vector2d*>& polygonVertices = polygonIterator->second;

    if ((unsigned int)vertexPosition >= polygonVertices.size())
      throw runtime_error("Vertex not found");

    return *polygonVertices[vertexPosition];
  }

  const Segment& GeometryFactory::GetPolygonEdge(const int& polygonId, const int& edgePosition)
  {
    const auto& polygonIterator = _polygonEdges.find(polygonId);

    if (polygonIterator == _polygonEdges.end())
      throw runtime_error("Polygon not found");

    const vector<const Segment*>& polygonEdges = polygonIterator->second;

    if ((unsigned int)edgePosition >= polygonEdges.size())
      throw runtime_error("Edge not found");

    return *polygonEdges[edgePosition];
  }

  double GeometryFactory::AreaRectangle(const vector<Vector2d> &vertices)
  {
      double base = (vertices[1]-vertices[0]).norm();
      double height = (vertices[2]-vertices[1]).norm();

      return base*height;
  }


 // Vector2d::Vector2d(const double& x, const double& y)
 // {
 //   _x = x;
 //   _y = y;
 // }

  Segment::Segment(const Vector2d& from, const Vector2d& to) : From(from), To(to) { }

}
