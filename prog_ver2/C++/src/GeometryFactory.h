#ifndef GEOMETRYFACTORY_H
#define GEOMETRYFACTORY_H

#include <iostream>
#include <list>
#include <unordered_map>
#include <vector>
#include "Eigen"

using namespace std;
using namespace Eigen;

namespace GeometryFactoryLibrary {

  //class Vector2d {
   // private:
    //  double _x;
    //  double _y;
  //  public:
  //    Vector2d(const double& x,
   //            const double& y);
   //   const double& X() const { return _x; }
    //  const double& Y() const { return _y; }
 // };

  class Point {
    public:
      double X;
      double Y;
  };

  class Segment {
    public:
      const Vector2d& From;
      const Vector2d& To;
      Segment(const Vector2d& from,
              const Vector2d& to);
  };

      class Polygon {
      };

      class IGeometryFactory {
        public:
          virtual int CreatePolygon(const vector<Vector2d>& vertices) = 0;
          virtual const Polygon& GetPolygon(const int& polygonId) = 0;
          virtual int GetPolygonNumberVertices(const int& polygonId) = 0;
          virtual const Vector2d& GetPolygonVertex(const int& polygonId,
                                                const int& vertexPosition) = 0;
          virtual const Segment& GetPolygonEdge(const int& polygonId,
                                                const int& edgePosition) = 0;
          virtual double AreaRectangle(const vector<Vector2d>& vertices) = 0;
      };

      class GeometryFactory : public IGeometryFactory {
        private:
          unordered_map<int, Polygon> _polygons;
          list<Vector2d> _points;
          list<Segment> _segments;
          unordered_map<int, vector<const Vector2d*>> _polygonVertices;
          unordered_map<int, vector<const Segment*>> _polygonEdges;

        public:
          int CreatePolygon(const vector<Vector2d>& vertices);
          const Polygon& GetPolygon(const int& polygonId);
          int GetPolygonNumberVertices(const int& polygonId);
          const Vector2d& GetPolygonVertex(const int& polygonId,
                                        const int& vertexPosition);
          const Segment& GetPolygonEdge(const int& polygonId,
                                        const int& edgePosition);
          double AreaRectangle(const vector<Vector2d>&vertices);

      };

  };

#endif // GEOMETRYFACTORY_H
