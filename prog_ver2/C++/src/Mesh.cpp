#include "Mesh.hpp"
#include "Cut.hpp"
#include "Intersector1D1D.h"
#include "GeometryFactory.h"
#include <list>

using namespace std;
namespace GeDiM {


vector<Vector2d> Mesh::ComputeBoundingBox(const vector<Vector2d> vertices)
{
    double max_x = vertices[0][0];
    double min_x = vertices[0][0];
    double max_y = vertices[0][1];
    double min_y = vertices[0][1];

    for(unsigned int i=1; i<vertices.size();i++){
        if(vertices[i][0]>max_x)
            max_x = vertices[i][0];
        if(vertices[i][0]<min_x)
            min_x = vertices[i][0];
        if(vertices[i][1]>max_y)
            max_y = vertices[i][1];
        if(vertices[i][1]<min_y)
            min_y = vertices[i][1];
    }
    vector<Vector2d> bB;
    bB.reserve(4);
    bB.push_back(Vector2d(min_x,min_y));
    bB.push_back(Vector2d(max_x,min_y));
    bB.push_back(Vector2d(max_x,max_y));
    bB.push_back(Vector2d(min_x,max_y));

    return bB;
}

vector<Vector2d> Mesh::CreateReferenceElement(const vector<Vector2d> polygon, const vector<Vector2d> bB)
{
    vector<Vector2d> referenceElement;
    referenceElement.reserve(polygon.size()+bB.size());

    for(unsigned int i=0;i<polygon.size();i++)
        referenceElement.push_back(polygon[i]);

    for(unsigned int i=0;i<bB.size();i++)
        referenceElement.push_back(bB[i]);

    return referenceElement;
}

vector<Vector2d> Mesh::TranslateToOrigin(const vector<Vector2d> referenceElement)
{
    vector<Vector2d> referenceElementTrans;
    Vector2d translation;
    translation[0] = referenceElement[referenceElement.size()-4][0];  //perch� ho concatenato i punti di bB a quelli del poligono
    translation[1] = referenceElement[referenceElement.size()-4][1];

    for(unsigned int i=0;i<referenceElement.size();i++){
        Vector2d newRef = referenceElement[i]-translation;
        referenceElementTrans.push_back(newRef);
    }
    return referenceElementTrans;
}

vector<Vector2d> Mesh::TranslationPoly(vector<Vector2d> referenceElement, bool direction)
{
    vector<Vector2d> verticesTrans;
    verticesTrans.reserve(referenceElement.size());
    if(direction){
        double length = (referenceElement[referenceElement.size()-1]-referenceElement[referenceElement.size()-2]).norm();
        for(unsigned int i=0;i<referenceElement.size();i++)
            verticesTrans.push_back(Vector2d(referenceElement[i][0]+length,referenceElement[i][1]));
    }

    if(!direction){
        double height = (referenceElement[referenceElement.size()-2]-referenceElement[referenceElement.size()-3]).norm();
        for(unsigned int i=0;i<referenceElement.size();i++)
            verticesTrans.push_back(Vector2d(referenceElement[i][0],referenceElement[i][1]+height));
    }

    return verticesTrans;
}

vector<int> Mesh::CreateMesh(const vector<Vector2d> Domain, const vector<Vector2d> referenceElement)
{
    vector<Vector2d> ref = static_cast<vector<Vector2d>>(referenceElement);
    Mesh mesh;
    GeometryFactory poly;
    vector<Vector2d> bB;
    vector<int> meshPolygons;
    int count = 0;
    double AreaMesh = 0;
    double domainLength = (Domain[0]-Domain[1]).norm();
    double domainHeight = (Domain[1]-Domain[2]).norm();
    bool direction = true;
    bool end = false;
    double bLength = (referenceElement[referenceElement.size()-1]-referenceElement[referenceElement.size()-2]).norm();
    double bHeight = (referenceElement[referenceElement.size()-2]-referenceElement[referenceElement.size()-3]).norm();
    double totalLength = bLength;
    double totalHeight = bHeight;
    double tollerance = 1.0e-6;

    //traslo il referenceElement nell'origine e aggiungo il primo tassello
    ref = mesh.TranslateToOrigin(ref);
    meshPolygons.push_back(poly.CreatePolygon(ref));

    for(unsigned int i=ref.size()-4; i<ref.size(); i++)
        bB.push_back(ref[i]);
    //aggiorno area della mesh
    AreaMesh += poly.AreaRectangle(bB);

    while(totalHeight <= domainHeight + tollerance && !end){
        while (direction) {
            if(totalLength <= domainLength + tollerance){
                
                ref = mesh.TranslationPoly(ref, direction);

                if(totalLength + bLength > domainLength){
                    bool side=false;
                    vector<Vector2d> polygon;
                    vector<Vector2d> bB;
                    vector<Vector2d> polygonCut;
                    vector<Vector2d> bBCut;
                    vector<Vector2d> newRef;
                    Cut cut;
                    GeometryFactory p;
                    double Area;

                    for(unsigned int i=0; i<ref.size()-4; i++)
                        polygon.push_back(ref[i]);
                    for(unsigned int i=ref.size()-4; i<ref.size(); i++)
                        bB.push_back(ref[i]);

                    //taglio referenceElement
                    polygonCut = cut.CutPolygon2(polygon,Domain[1], Domain[2], side);
                    bBCut = cut.CutPolygon2(bB,Domain[1], Domain[2], side);

                    vector<Vector2d> bBCutArea;
                    bBCutArea.push_back(Vector2d(bBCut[0][0],bBCut[0][1]));
                    bBCutArea.push_back(Vector2d(bBCut[2][0],bBCut[2][1]));
                    bBCutArea.push_back(Vector2d(bBCut[3][0],bBCut[3][1]));
                    bBCutArea.push_back(Vector2d(bBCut[1][0],bBCut[1][1]));

                    //calcolo area referenceElement tagliato
                    Area = p.AreaRectangle(bBCutArea);

                    for(unsigned int i=0; i<polygonCut.size(); i++)
                        newRef.push_back(polygonCut[i]);
                    //for(unsigned int i=polygonCut.size(); i<bBCut.size()+polygonCut.size(); i++)
                       // newRef.push_back(bBCutArea[i]);
                    newRef.push_back(Vector2d(bBCut[0][0],bBCut[0][1]));
                    newRef.push_back(Vector2d(bBCut[2][0],bBCut[2][1]));
                    newRef.push_back(Vector2d(bBCut[3][0],bBCut[3][1]));
                    newRef.push_back(Vector2d(bBCut[1][0],bBCut[1][1]));

                 //aggiungo tassello tagliato
                 meshPolygons.push_back(poly.CreatePolygon(newRef));
                 AreaMesh+=Area;
                }
                else{
                  meshPolygons.push_back(poly.CreatePolygon(ref));
                  vector<Vector2d> bBArea;
                  bBArea.push_back(Vector2d(ref[ref.size()-4][0],ref[ref.size()-4][1]));
                  bBArea.push_back(Vector2d(ref[ref.size()-3][0],ref[ref.size()-3][1]));
                  bBArea.push_back(Vector2d(ref[ref.size()-2][0],ref[ref.size()-2][1]));
                  bBArea.push_back(Vector2d(ref[ref.size()-1][0],ref[ref.size()-1][1]));

                  AreaMesh += poly.AreaRectangle(bBArea);}

                totalLength += bLength;
            }
            else
                direction = false;

            if(totalHeight >= domainHeight)
                end = true;
        }
        if(direction == false && totalHeight < domainHeight){
            vector<Vector2d> ref_v = static_cast<vector<Vector2d>>(referenceElement);
            ref_v = mesh.TranslateToOrigin(ref_v);
            
            for(int j=0;j<count+1;j++)
                ref_v = mesh.TranslationPoly(ref_v, direction);

            if(totalHeight+bHeight > domainHeight + tollerance){
                bool side=true;
                vector<Vector2d> polygon;
                vector<Vector2d> bB;
                vector<Vector2d> polygonCut;
                vector<Vector2d> bBCut;
                Cut cut;
                GeometryFactory p;
                double Area;

                for(unsigned int i=0; i<ref_v.size()-4; i++)
                    polygon.push_back(ref_v[i]);
                for(unsigned int i=ref_v.size()-4; i<ref_v.size(); i++)
                    bB.push_back(ref_v[i]);

                //taglio referenceElement
                polygonCut = cut.CutPolygon2(polygon, Domain[3], Domain[2], side);
                bBCut = cut.CutPolygon2(bB, Domain[3], Domain[2], side);

                //calcolo area referenceElement tagliato
                Area = p.AreaRectangle(bBCut);

                ref.clear();
                for(unsigned int i=0; i<polygonCut.size(); i++)
                    ref.push_back(polygonCut[i]);
                ref.push_back(Vector2d(bBCut[0][0], bBCut[0][1]));
                ref.push_back(Vector2d(bBCut[1][0], bBCut[1][1]));
                ref.push_back(Vector2d(bBCut[2][0], bBCut[2][1]));
                ref.push_back(Vector2d(bBCut[3][0], bBCut[3][1]));

             //aggiungo tassello tagliato
             meshPolygons.push_back(poly.CreatePolygon(ref));


             totalHeight+=bBCut[2][1]-bBCut[1][1];
             AreaMesh+=Area;
            }
            else{
              meshPolygons.push_back(poly.CreatePolygon(ref));
              ref = ref_v;
              AreaMesh += poly.AreaRectangle(bB);
              totalHeight += bHeight;}

            direction = true;
            totalLength = bLength;
            count++;
        }
    }

    double AreaDomain = poly.AreaRectangle(Domain);

    cout<<"Area Dominio = "<<AreaDomain<<endl;
    cout<<"Area Mesh = "<<AreaMesh<<endl;

    cout<<"Numero tasselli utilizzati:"<<meshPolygons.size()<<endl;
    for(unsigned int j=0;j<meshPolygons.size();j++){
        cout<<" "<<endl;
        cout<<"Piastrella "<<j+1<<endl;
        for(int i=0;i<poly.GetPolygonNumberVertices(meshPolygons[j]);i++){
            cout<<"vertice\t"<<i<<endl;
            cout<<poly.GetPolygonVertex(meshPolygons[j],i)[0]<<"\t"<<poly.GetPolygonVertex(meshPolygons[j],i)[1]<<endl;
        }
    }
    return meshPolygons;
}

}
