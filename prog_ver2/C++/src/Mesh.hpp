#ifndef MESH_H
#define MESH_H

#include "GeometryFactory.h"
#include "Intersector1D1D.h"
#include "Cut.hpp"

using namespace GeometryFactoryLibrary;

namespace GeDiM {

  class Mesh;

  class Mesh {
  private:

    public:

        vector<Vector2d> ComputeBoundingBox(const vector<Vector2d> vertices);
        vector<Vector2d> CreateReferenceElement(const vector<Vector2d> polygon, const vector<Vector2d> bB);
        vector<Vector2d> TranslateToOrigin(const vector<Vector2d> referenceElement);
        vector<Vector2d> TranslationPoly(vector<Vector2d> referenceElement, bool direction);
        vector<int> CreateMesh(const vector<Vector2d> Domain, const vector<Vector2d> referenceElement);
  };
}

#endif // MESH_H
