#ifndef __TEST_CUT_H
#define __TEST_CUT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Cut.hpp"
#include "GeometryFactory.h"


using namespace testing;
using namespace std;
using namespace GeDiM;

namespace CutTesting {

void FillPolygonVerticesOne(vector<Vector2d>& points)
{
    points.reserve(4);
    points.push_back(Vector2d(1.0000e+00, 1.0000e+00));
    points.push_back(Vector2d(5.0000e+00, 1.0000e+00));
    points.push_back(Vector2d(5.0000e+00, 3.1000e+00));
    points.push_back(Vector2d(1.0000e+00, 3.1000e+00));
}

void FillSegmentOne(Vector2d& origin, Vector2d& end)
{
    origin[0]=2.0;
    origin[1]=1.2;
    end[0]=4.0;
    end[1]=3.0;
}

void FillPolygonVerticesTwo(vector<Vector2d>& points)
{
    points.reserve(5);
    points.push_back(Vector2d(2.5000e+00, 1.0000e+00));
    points.push_back(Vector2d(4.0000e+00, 2.1000e+00));
    points.push_back(Vector2d(3.4000e+00, 4.2000e+00));
    points.push_back(Vector2d(1.6000e+00, 4.2000e+00));
    points.push_back(Vector2d(1.0000e+00, 2.1000e+00));
}

void FillSegmentTwo(Vector2d& origin, Vector2d& end)
{
    origin[0]=1.4;
    origin[1]=2.75;
    end[0]=3.6;
    end[1]=2.2;
}

void FillPolygonVerticesThree(vector<Vector2d>& points)
{
    points.reserve(6);
    points.push_back(Vector2d(1.5, 1.0));
    points.push_back(Vector2d(5.6, 1.5));
    points.push_back(Vector2d(5.5, 4.8));
    points.push_back(Vector2d(4.0, 6.2));
    points.push_back(Vector2d(3.2, 4.2));
    points.push_back(Vector2d(1.0, 4.0));
}

void FillSegmentThree(Vector2d& origin, Vector2d& end)
{
    origin[0]=2.0;
    origin[1]=3.7;
    end[0]=4.1;
    end[1]=5.9;
}

void FillPolygonVerticesFour(vector<Vector2d>& points)
{
    points.reserve(6);
    points.push_back(Vector2d(2.5, 1.0));
    points.push_back(Vector2d(4.2, 1.6));
    points.push_back(Vector2d(5.1, 3.5));
    points.push_back(Vector2d(4.8, 4.2));
    points.push_back(Vector2d(3.0, 5.5));
    points.push_back(Vector2d(2.0, 4.0));
}

void FillSegmentFour(Vector2d& origin, Vector2d& end)
{
    origin[0]=2.5;
    origin[1]=3.0;
    end[0]=4.5;
    end[1]=3.5;
}

void FillPolygonVerticesFive(vector<Vector2d>& points)
{
    points.reserve(5);
    points.push_back(Vector2d(1.0, 0.5));
    points.push_back(Vector2d(3.5, 3.3));
    points.push_back(Vector2d(2.0, 2.5));
    points.push_back(Vector2d(0.6, 3.8));
    points.push_back(Vector2d(1.5, 2.0));
}

void FillSegmentFive(Vector2d& origin, Vector2d& end)
{
    origin[0]=1.2;
    origin[1]=3.2;
    end[0]=2.5;
    end[1]=2.8;
}

TEST(TestCut, TestCutPolygonOne)
{
    vector<Vector2d> points;
    FillPolygonVerticesOne(points);
    Vector2d origin;
    Vector2d end;
    FillSegmentOne(origin,end);

    Cut newCut;
    newCut.CutPolygon(points, origin, end, true);
}


TEST(TestCut, TestCutPolygonTwo)
{
    vector<Vector2d> points;
    FillPolygonVerticesTwo(points);
    Vector2d origin;
    Vector2d end;
    FillSegmentTwo(origin,end);

    Cut newCut;
    newCut.CutPolygon(points, origin, end, true);

}

TEST(TestCut, TestCutPolygonThree)
{
    vector<Vector2d> points;
    FillPolygonVerticesThree(points);
    Vector2d origin;
    Vector2d end;
    FillSegmentThree(origin,end);

    Cut newCut;
    newCut.CutPolygon(points, origin, end, true);

}

TEST(TestCut, TestCutPolygonFour)
{
    vector<Vector2d> points;
    FillPolygonVerticesFour(points);
    Vector2d origin;
    Vector2d end;
    FillSegmentFour(origin,end);

    Cut newCut;
    newCut.CutPolygon(points, origin, end, true);

}

TEST(TestCut, TestCutPolygonFive)
{
    vector<Vector2d> points;
    FillPolygonVerticesFive(points);
    Vector2d origin;
    Vector2d end;
    FillSegmentFive(origin,end);

    Cut newCut;
    newCut.CutPolygon(points, origin, end, true);

}
}

#endif // __TEST_CUT_H
