#ifndef __MESH_CUT_H
#define __MESH_CUT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "Mesh.hpp"
#include "Cut.hpp"
#include "GeometryFactory.h"


using namespace testing;
using namespace std;
using namespace GeDiM;

namespace MeshTesting {

void FillPolygonVertices(vector<Vector2d>& points)
{
    points.reserve(6);
    points.push_back(Vector2d(1.5, 1.0));
    points.push_back(Vector2d(5.6, 1.5));
    points.push_back(Vector2d(5.5, 4.8));
    points.push_back(Vector2d(4.0, 6.2));
    points.push_back(Vector2d(3.2, 4.2));
    points.push_back(Vector2d(1.0, 4.0));
}

void FillDomainVertices(vector<Vector2d>& points)
{
    points.reserve(4);
    points.push_back(Vector2d(0.0, 0.0));
    points.push_back(Vector2d(19.4, 0.0));
    points.push_back(Vector2d(19.4, 16.6));
    points.push_back(Vector2d(0.0, 16.6));
}

TEST(TestMesh, TestComputeBoundingBox)
{
  vector<Vector2d> vertices;
  FillPolygonVertices(vertices);

  Mesh mesh;

  try
  {

    EXPECT_EQ(mesh.ComputeBoundingBox(vertices)[2][1], 6.2);

  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestMesh, TestCreateReferenceElement)
{
  vector<Vector2d> vertices;
  FillPolygonVertices(vertices);
  vector<Vector2d> bB;
  vector<Vector2d> ref;

  Mesh mesh;
  bB=mesh.ComputeBoundingBox(vertices);

  ref.reserve(vertices.size()+bB.size());
  for(unsigned int i=0; i<vertices.size(); i++)
      ref.push_back(Vector2d(vertices[i][0], vertices[i][1]));
  for(unsigned int i=0; i<bB.size(); i++)
      ref.push_back(Vector2d(bB[i][0], bB[i][1]));

  try
  {

    EXPECT_EQ(mesh.CreateReferenceElement(vertices, bB), ref);

  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestMesh, TestTranslationToOrigin)
{
  vector<Vector2d> vertices;
  FillPolygonVertices(vertices);

  Mesh mesh;
  vector<Vector2d> bB;
  vector<Vector2d> referenceElement;

  bB=mesh.ComputeBoundingBox(vertices);
  referenceElement=mesh.CreateReferenceElement(vertices,bB);

  try
  {

    EXPECT_EQ(mesh.TranslateToOrigin(referenceElement)[mesh.TranslateToOrigin(referenceElement).size()-4][0], 0);
    EXPECT_EQ(mesh.TranslateToOrigin(referenceElement)[mesh.TranslateToOrigin(referenceElement).size()-4][1], 0);

  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestMesh, TestTranslationPoly)
{
  vector<Vector2d> vertices;
  FillPolygonVertices(vertices);

  Mesh mesh;
  vector<Vector2d> bB;
  vector<Vector2d> referenceElement;
  bool direction=true;

  bB=mesh.ComputeBoundingBox(vertices);
  referenceElement=mesh.CreateReferenceElement(vertices,bB);

  try
  {

    EXPECT_EQ(mesh.TranslationPoly(referenceElement,direction)[0][0], 1.5+4.6);

  }
  catch (const exception& exception)
  {
    FAIL();
  }
}

TEST(TestMesh, TestCreateMesh)
{
  vector<Vector2d> vertices;
  vector<Vector2d> domain;
  vector<int> poly;
  FillPolygonVertices(vertices);
  FillDomainVertices(domain);

  Mesh mesh;
  vector<Vector2d> bB;
  vector<Vector2d> referenceElement;
  bool direction=true;

  bB=mesh.ComputeBoundingBox(vertices);
  referenceElement=mesh.CreateReferenceElement(vertices,bB);

  poly = mesh.CreateMesh(domain, referenceElement);

  try
  {

    EXPECT_EQ(mesh.TranslationPoly(referenceElement,direction)[0][0], 1.5+4.6);

  }
  catch (const exception& exception)
  {
    FAIL();
  }
}
}

#endif // __MESH_CUT_H
