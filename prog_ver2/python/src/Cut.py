import numpy as np
import src.Intersector1D1D as intersectorLibrary


class Cut:
    def __init__(self):
        self.newPoints = []
        self.counterwiseVertices = []
        self.intersectionPoints = []
        self.cuttedPolygon = []
        self.reverseCuttedPolygons = []
        self.cuttedPolygons = []
        self.resultPolygons = []

    def CutPolygon(self, points: np.array, polygonVertices: np.array, origin: np.array, end: np.array):

        for i in range(0, len(points)):
            self.newPoints.append(points[i])

        intersection = intersectorLibrary.Intersector1D1D(1.0e-7, 1.0e-7)
        intersection.SetFirstSegment(origin, end)
        tolleranza = 1.0e-6

        for i in range(0, len(points)-1):
            self.counterwiseVertices.append(points[i])

            intersection.SetSecondSegment(points[i], points[i+1])
            intersection.ComputeIntersection()
            if intersection.TypeIntersection() == intersectorLibrary.Type.IntersectionOnLine or intersection.TypeIntersection() == intersectorLibrary.Type.IntersectionOnSegment:
                point = np.zeros(2)
                t = end-origin
                param = intersection.ParametricCoordinates()
                point[0] = origin[0] + t[0] * param[0]
                point[1] = origin[1] + t[1] * param[0]

                #if (point != points[i]).all and (point != points[i+1]).all:
                if abs(point[0]-points[i][0])/abs(point[0]) >= tolleranza or abs(point[1]-points[i][1])/abs(point[1]) >= tolleranza:
                    if abs(point[0]-points[i+1][0])/abs(point[0]) >= tolleranza or abs(point[1]-points[i+1][1])/abs(point[1]) >= tolleranza:
                        self.intersectionPoints.append(point)
                        self.counterwiseVertices.append(point)

        self.counterwiseVertices.append(points[len(points)-1])
        intersection.SetSecondSegment(points[len(points)-1], points[0])
        intersection.ComputeIntersection()
        if intersection.TypeIntersection() == intersectorLibrary.Type.IntersectionOnLine or intersection.TypeIntersection() == intersectorLibrary.Type.IntersectionOnSegment:
            point = np.zeros(2)
            t = end - origin
            param = intersection.ParametricCoordinates()
            point[0] = origin[0] + t[0] * param[0]
            point[1] = origin[1] + t[1] * param[0]

            #if (point != points[len(points)-1]).all and (point != points[0]).all:
            if abs(point[0] - points[len(points)-1][0]) / abs(point[0]) >= tolleranza and abs(point[1] - points[len(points)-1][1]) / abs(point[1]) >= tolleranza:
                if abs(point[0] - points[0][0]) / abs(point[0]) >= tolleranza and abs(point[1] - points[0][1]) / abs(point[1]) >= tolleranza:
                    self.intersectionPoints.append(point)
                    self.counterwiseVertices.append(point)

        if self.intersectionPoints[0][0]<origin[0] and self.intersectionPoints[len(self.intersectionPoints)-1][0]>end[0]:
            self.newPoints.append(self.intersectionPoints[0])
            self.newPoints.append(origin)
            for i in range(1, len(self.intersectionPoints)-1):
                self.newPoints.append(self.intersectionPoints[i])
            self.newPoints.append(end)
            self.newPoints.append(self.intersectionPoints[len(self.intersectionPoints)-1])

        if self.intersectionPoints[0][0]>end[0] and self.intersectionPoints[len(self.intersectionPoints)-1][0]<origin[0]:
            self.newPoints.append(self.intersectionPoints[0])
            self.newPoints.append(end)
            for i in range(1, len(self.intersectionPoints)-1):
                self.newPoints.append(self.intersectionPoints[i])
            self.newPoints.append(origin)
            self.newPoints.append(self.intersectionPoints[len(self.intersectionPoints)-1])

        if len(self.intersectionPoints) == 1:
            self.newPoints.append(end)
            self.newPoints.append(origin)
            self.newPoints.append(self.intersectionPoints[0])

        segmento = origin - end
        for i in range(0, len(self.newPoints)):
            differenza = origin - self.newPoints[i]
            determinante = segmento[0]*differenza[1] - differenza[0]*segmento[1]
            tollerance = 1.0e-14

            if determinante <= tollerance:
                self.cuttedPolygon.append(self.newPoints[i])
            if determinante >= -tollerance:
                self.reverseCuttedPolygons.append(self.newPoints[i])

        for i in range(0, len(self.cuttedPolygon)):
            for j in range(0, len(self.newPoints)):
                if self.cuttedPolygon[i][0] == self.newPoints[j][0] and self.cuttedPolygon[i][1] == self.newPoints[j][1]:
                    self.cuttedPolygons.append(j)

        if len(self.intersectionPoints) <= 2:
            for i in range(0, len(self.reverseCuttedPolygons)):
                for j in range(0, len(self.newPoints)):
                    if self.reverseCuttedPolygons[i][0] == self.newPoints[j][0] and self.reverseCuttedPolygons[i][1] == self.newPoints[j][1]:
                        self.cuttedPolygons.append(j)

        if len(self.intersectionPoints) > 2:
            self.resultPolygons.append(self.reverseCuttedPolygons[2])
            self.resultPolygons.append(self.reverseCuttedPolygons[0])
            self.resultPolygons.append(self.reverseCuttedPolygons[4])
            self.resultPolygons.append(self.reverseCuttedPolygons[3])

            self.resultPolygons.append(self.reverseCuttedPolygons[5])
            self.resultPolygons.append(self.reverseCuttedPolygons[1])
            self.resultPolygons.append(self.reverseCuttedPolygons[7])
            self.resultPolygons.append(self.reverseCuttedPolygons[6])

            for i in range(0, len(self.resultPolygons)):
                for j in range(0, len(self.newPoints)):
                    if self.resultPolygons[i][0] == self.newPoints[j][0] and self.resultPolygons[i][1] == self.newPoints[j][1]:
                        self.cuttedPolygons.append(j)


        print("the collection of resulting new points created concatenated with the original collection is:")
        for i in range(0, len(self.newPoints)):
            print("Point", i)
            print("%.1f" %self.newPoints[i][0], "%.1f" %self.newPoints[i][1], sep='   ')
            print()

        print("the resulting N polygons derived from the cutting process as a collection of vertices indices ordered counterclockwise are:")
        if len(self.intersectionPoints) <= 2:
            print("Polygon 1:")
            for i in range(0, len(self.cuttedPolygon)):
                print(self.cuttedPolygons[i])
            print()
            print("Polygon 2:")
            for i in range(len(self.cuttedPolygon), len(self.cuttedPolygons)):
                print(self.cuttedPolygons[i])
            print()
        else:
            print("Polygon 1:")
            for i in range(0, len(self.cuttedPolygon)):
                print(self.cuttedPolygons[i])
            print()
            print("Polygon 2:")
            for i in range(len(self.cuttedPolygon), len(self.cuttedPolygons)-4):
                print(self.cuttedPolygons[i])
            print()
            print("Polygon 3:")
            for i in range(len(self.cuttedPolygons)-4, len(self.cuttedPolygons)):
                print(self.cuttedPolygons[i])
            print()

        pass
