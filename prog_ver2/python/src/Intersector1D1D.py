import numpy as np
from enum import Enum


class Position(Enum):
    Begin = 0
    Inner = 1
    End = 2
    Outer = 3


class Type(Enum):
    NoIntersection = 0
    IntersectionOnLine = 1
    IntersectionOnSegment = 2
    IntersectionParallelOnLine = 3
    IntersectionParallelOnSegment = 4


class Intersector1D1D:

    def __init__(self, toleranceParallelism: float, toleranceIntersection: float):
        self.matrixTangentVector = np.zeros((2, 2))
        self.__toleranceParallelism = toleranceParallelism
        self.__toleranceIntersection = toleranceIntersection

    def SetToleranceParallelism(self, _tolerance: float):
        self.__toleranceParallelism = _tolerance
        pass

    def SetToleranceIntersection(self, _tolerance: float):
        self.__toleranceIntersection = _tolerance
        pass

    def SetFirstSegment(self, origin: np.array, end: np.array):
        self.matrixTangentVector[0][0] = end[0] - origin[0]
        self.matrixTangentVector[1][0] = end[1] - origin[1]
        self.originFirstSegment = origin
        pass

    def SetSecondSegment(self, origin: np.array, end: np.array):
        self.matrixTangentVector[0][1] = origin[0] - end[0]
        self.matrixTangentVector[1][1] = origin[1] - end[1]
        self.rightHandSide = origin - self.originFirstSegment
        pass

    def ComputeIntersection(self) -> bool:
        self.type = Type.NoIntersection
        parallelism = np.linalg.det(self.matrixTangentVector)
        self.intersection = False
        col0 = np.zeros(2)
        col0[0] = self.matrixTangentVector[0][0]
        col0[1] = self.matrixTangentVector[1][0]
        normaprimacolonna = np.linalg.norm(col0)
        col1 = np.zeros(2)
        col1[0] = self.matrixTangentVector[0][1]
        col1[1] = self.matrixTangentVector[1][1]
        normasecondacolonna = np.linalg.norm(col1)

        check = self.__toleranceParallelism * self.__toleranceParallelism * normaprimacolonna * normaprimacolonna * normasecondacolonna * normasecondacolonna
        if parallelism * parallelism >= check:
            solverMatrix = np.zeros((2, 2))
            solverMatrix[0][0] = self.matrixTangentVector[1][1]
            solverMatrix[0][1] = - self.matrixTangentVector[0][1]
            solverMatrix[1][0] = - self.matrixTangentVector[1][0]
            solverMatrix[1][1] = self.matrixTangentVector[0][0]
            self.resultParametricCoordinates = np.dot(solverMatrix, self.rightHandSide)
            self.resultParametricCoordinates = self.resultParametricCoordinates/parallelism

            if self.resultParametricCoordinates[1] > -self.__toleranceIntersection and self.resultParametricCoordinates[1] - 1.0 < self.__toleranceIntersection:
                self.type = Type.IntersectionOnLine
                self.intersection = True
                if self.resultParametricCoordinates[0] > -self.__toleranceIntersection and self.resultParametricCoordinates[0] - 1.0 < self.__toleranceIntersection:
                    self.type = Type.IntersectionOnSegment
        else:
            parallelism2 = abs(self.matrixTangentVector[0][0] * self.rightHandSide[1] - self.rightHandSide[0] * self.matrixTangentVector[1][0])
            col0 = np.zeros(2)
            col0[0] = self.matrixTangentVector[0][0]
            col0[1] = self.matrixTangentVector[1][0]
            col1 = np.zeros(2)
            col1[0] = self.matrixTangentVector[0][1]
            col1[1] = self.matrixTangentVector[1][1]
            squaredNormFirstEdge = np.linalg.norm(col0)
            normRightHandSide = np.linalg.norm(self.rightHandSide)
            check2 = self.__toleranceParallelism * self.__toleranceParallelism * squaredNormFirstEdge * squaredNormFirstEdge * normRightHandSide * normRightHandSide
            if parallelism2*parallelism2 <= check2:
                tempNorm = 1.0/(squaredNormFirstEdge * squaredNormFirstEdge)
                self.resultParametricCoordinates = np.zeros(2)
                self.resultParametricCoordinates[0] = np.inner(col0, self.rightHandSide) * tempNorm
                self.resultParametricCoordinates[1] = self.resultParametricCoordinates[0] - np.inner(col0,col1) * tempNorm

                self.intersection = True
                self.type = Type.IntersectionParallelOnLine

                if self.resultParametricCoordinates[1] < self.resultParametricCoordinates[0]:
                    tmp = self.resultParametricCoordinates[0]
                    self.resultParametricCoordinates[0] = self.resultParametricCoordinates[1]
                    self.resultParametricCoordinates[1] = tmp
                if (self.resultParametricCoordinates[0] > -self.__toleranceIntersection and self.resultParametricCoordinates[0] - 1.0 < self.__toleranceIntersection) or (self.resultParametricCoordinates[1] > -self.__toleranceIntersection and self.resultParametricCoordinates[1]-1.0 < self.__toleranceIntersection):
                    self.type = Type.IntersectionParallelOnSegment
                else:
                    if self.resultParametricCoordinates[0] < self.__toleranceIntersection and self.resultParametricCoordinates[1] - 1.0 > -self.__toleranceIntersection:
                        self.type = Type.IntersectionParallelOnSegment

        if self.resultParametricCoordinates[0] < -self.__toleranceIntersection or self.resultParametricCoordinates[0] > 1.0 + self.__toleranceIntersection:
            self.positionIntersectionFirstEdge = Position.Outer
        elif self.resultParametricCoordinates[0] > -self.__toleranceIntersection and self.resultParametricCoordinates[0] < self.__toleranceIntersection:
            self.resultParametricCoordinates[0] = 0.0
            self.positionIntersectionFirstEdge = Position.Begin
        elif self.resultParametricCoordinates[0] > 1.0-self.__toleranceIntersection and self.resultParametricCoordinates[0] < 1.0+self.__toleranceIntersection:
            self.resultParametricCoordinates[0] = 1.0
            self.positionIntersectionFirstEdge = Position.End
        else:
            self.positionIntersectionFirstEdge = Position.Inner

        if self.resultParametricCoordinates[1] < -self.__toleranceIntersection or self.resultParametricCoordinates[1] > 1.0 + self.__toleranceIntersection:
            self.positionIntersectionSecondEdge = Position.Outer
        elif self.resultParametricCoordinates[1] > -self.__toleranceIntersection and self.resultParametricCoordinates[1] < self.__toleranceIntersection:
            self.resultParametricCoordinates[1] = 0.0
            self.positionIntersectionSecondEdge = Position.Begin
        elif self.resultParametricCoordinates[1] > 1.0-self.__toleranceIntersection and self.resultParametricCoordinates[1] < 1.0+self.__toleranceIntersection:
            self.resultParametricCoordinates[1] = 1.0
            self.positionIntersectionSecondEdge = Position.End
        else:
            self.positionIntersectionSecondEdge = Position.Inner

        return self.intersection

    def ToleranceIntersection(self) -> float:
        return self.__toleranceIntersection

    def ToleranceParallelism(self) -> float:
        return self.__toleranceParallelism

    def ParametricCoordinates(self) -> np.array:
        return self.resultParametricCoordinates

    def FirstParametricCoordinate(self) -> float:
        return self.resultParametricCoordinates[0]

    def SecondParametricCoordinate(self) -> float:
        return self.resultParametricCoordinates[1]

    def IntersectionFirstParametricCoordinate(self, origin: np.array, end: np.array) -> np.array:
        self.IntFirst = (1-self.resultParametricCoordinates[0])*origin + self.resultParametricCoordinates[0]*end
        return self.IntFirst

    def IntersectionSecondParametricCoordinate(self, origin: np.array, end: np.array) -> np.array:
        self.IntSecond = (1-self.resultParametricCoordinates[1])*origin + self.resultParametricCoordinates[0]*end   #non dovrebbe essere 1?
        return self.IntSecond

    def PositionIntersectionInFirstEdge(self) -> Position:
        return self.positionIntersectionFirstEdge

    def PositionIntersectionInSecondEdge(self) -> Position:
        return self.positionIntersectionSecondEdge

    def TypeIntersection(self) -> Type:
        return self.type