from unittest import TestCase

import src.Cut as cutLibrary
import numpy as np


class TestCut(TestCase):

    def test_cut_polygon_one(self):
        origin = np.array([2.0, 1.2])
        end = np.array([4.0, 3.0])
        points = []
        points.append([1.0, 1.0])
        points.append([5.0, 1.0])
        points.append([5.0, 3.1])
        points.append([1.0, 3.1])

        self.polygonVertices = []
        for i in range(0, len(points)):
            self.polygonVertices.append(i)

        newCut = cutLibrary.Cut()
        newCut.CutPolygon(points, self.polygonVertices, origin, end)

    def test_cut_polygon_two(self):
        origin = np.array([1.4, 2.75])
        end = np.array([3.6, 2.2])
        points = []
        points.append([2.5, 1.0])
        points.append([4.0, 2.1])
        points.append([3.4, 4.2])
        points.append([1.6, 4.2])
        points.append([1.0, 2.1])

        self.polygonVertices = []
        for i in range(0, len(points)):
            self.polygonVertices.append(i)

        newCut = cutLibrary.Cut()
        newCut.CutPolygon(points, self.polygonVertices, origin, end)

    def test_cut_polygon_three(self):
        origin = np.array([2.0, 3.7])
        end = np.array([4.1, 5.9])
        points = []
        points.append([1.5, 1.0])
        points.append([5.6, 1.5])
        points.append([5.5, 4.8])
        points.append([4.0, 6.2])
        points.append([3.2, 4.2])
        points.append([1.0, 4.0])

        self.polygonVertices = []
        for i in range(0, len(points)):
            self.polygonVertices.append(i)

        newCut = cutLibrary.Cut()
        newCut.CutPolygon(points, self.polygonVertices, origin, end)

    def test_cut_polygon_four(self):
        origin = np.array([2.5, 3.0])
        end = np.array([4.5, 3.5])
        points = []
        points.append([2.5, 1.0])
        points.append([4.2, 1.6])
        points.append([5.1, 3.5])
        points.append([4.8, 4.2])
        points.append([3.0, 5.5])
        points.append([2.0, 4.0])

        self.polygonVertices = []
        for i in range(0, len(points)):
            self.polygonVertices.append(i)

        newCut = cutLibrary.Cut()
        newCut.CutPolygon(points, self.polygonVertices, origin, end)

    def test_cut_polygon_five(self):
        origin = np.array([1.2, 3.2])
        end = np.array([2.5, 2.8])
        points = []
        points.append([1.0, 0.5])
        points.append([3.5, 3.3])
        points.append([2.0, 2.5])
        points.append([0.6, 3.8])
        points.append([1.5, 2.0])

        self.polygonVertices = []
        for i in range(0, len(points)):
            self.polygonVertices.append(i)

        newCut = cutLibrary.Cut()
        newCut.CutPolygon(points, self.polygonVertices, origin, end)





