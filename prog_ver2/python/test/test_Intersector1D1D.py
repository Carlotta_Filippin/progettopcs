from unittest import TestCase

import src.Intersector1D1D as intersectorLibrary
import numpy as np


class TestIntersector1D1D(TestCase):

    def test_parallel_intersection(self):
        a = np.array([0, 0])
        b = np.array([4, 0])
        c = np.array([1, 0])
        d = np.array([2, 0])
        intersectorNew = intersectorLibrary.Intersector1D1D(1.0e-7, 1.0e-7)

        intersectorNew.SetFirstSegment(a, b)
        intersectorNew.SetSecondSegment(c, d)
        self.assertTrue(intersectorNew.ComputeIntersection())
        self.assertEqual(intersectorNew.TypeIntersection(), intersectorLibrary.Type.IntersectionParallelOnSegment)
        self.assertAlmostEqual(0.25, intersectorNew.FirstParametricCoordinate())
        self.assertAlmostEqual(0.5, intersectorNew.SecondParametricCoordinate())

    def test_segment_intersection(self):
        a = np.array([1, 0])
        b = np.array([5, 0])
        c = np.array([3, -6])
        d = np.array([3, 6])
        intersector = intersectorLibrary.Intersector1D1D(1.0e-7, 1.0e-7)

        intersector.SetFirstSegment(a, b)
        intersector.SetSecondSegment(c, d)
        self.assertTrue(intersector.ComputeIntersection())
        self.assertEqual(intersector.TypeIntersection(), intersectorLibrary.Type.IntersectionOnSegment)
        self.assertAlmostEqual(0.5, intersector.FirstParametricCoordinate())
        self.assertAlmostEqual(0.5, intersector.SecondParametricCoordinate())
        self.assertLessEqual(intersector.FirstParametricCoordinate(), 1.0)
        self.assertGreaterEqual(intersector.FirstParametricCoordinate(), 0.0)

    def test_on_line_intersection(self):
        a = np.array([3, 6])
        b = np.array([3, 2])
        c = np.array([5, 0])
        d = np.array([1, 0])
        intersector = intersectorLibrary.Intersector1D1D(1.0e-7, 1.0e-7)

        intersector.SetFirstSegment(a, b)
        intersector.SetSecondSegment(c, d)
        self.assertTrue(intersector.ComputeIntersection())
        self.assertEqual(intersector.TypeIntersection(), intersectorLibrary.Type.IntersectionOnLine)
        self.assertGreaterEqual(intersector.FirstParametricCoordinate(), 1.0)
        self.assertAlmostEqual(1.5, intersector.FirstParametricCoordinate())
        self.assertAlmostEqual(0.5, intersector.SecondParametricCoordinate())